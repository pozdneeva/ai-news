import pandas as pd
from flask import Flask, request, jsonify
from clear_text import clear_text
from drop_duplicates import drop_duplicates_custom
from classifier import make_classes


# def predict():
#     df = pd.read_excel('data/ai_bot_app_post.xls')[['telegram_id', 'text']]
#     print('---- raw data:', df.shape)
#
#     # clear text
#     df = clear_text(df)
#     print('---- clear data:', df.shape)
#
#     # drop duplicates
#     df_without_duplicates = drop_duplicates_custom(df.head(1000))
#     print('---- data without duplicates:', df_without_duplicates.shape)
#
#     df_final = df.merge(df_without_duplicates, on='text').drop_duplicates(subset='text')
#     print('---- FINAL data:', df_final.shape)
#     df_final.to_excel('data/RESULT.xlsx', index=False)


# app = Flask('ai-news')
app = Flask(__name__)

@app.route('/')
def predict_endpoint():
    df = pd.read_excel('data/ai_bot_app_post.xls')[['telegram_id', 'text']]
    print('---- raw data:', df.shape)

    # clear text
    df = clear_text(df)
    print('---- clear data:', df.shape)

    # drop duplicates
    df_without_duplicates = drop_duplicates_custom(df.head(100))
    print('---- data without duplicates:', df_without_duplicates.shape)

    df_to_classify = df.merge(df_without_duplicates, on='text').drop_duplicates(subset='text')
    print('---- FINAL data:', df_to_classify.shape)

    # run classifier
    df_final = make_classes(df_to_classify)

    # df_final.to_excel('data/RESULT.xlsx', index=False)
    df_final.to_excel('/app/RESULT.xlsx', index=False)
    return '<h1 style="color: #003f8c"> done! </h1>'


# if __name__ == "__main__":
#     app.run(debug=True, host='0.0.0.0', port=9696)