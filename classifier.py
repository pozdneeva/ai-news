import torch
from datasets import load_dataset, Dataset
from transformers import (
    DistilBertForSequenceClassification,
    DistilBertTokenizerFast,
    TrainingArguments,
    Trainer,
    AutoConfig,
    AutoModelForSequenceClassification,
    TextClassificationPipeline
)
from sklearn.metrics import accuracy_score, f1_score
import numpy as np
import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd


def make_classes(df):
    final_map = {
        'блоги': 'Блоги',
        'новости_сми': 'Новости и СМИ',
        'юмор': 'Развлечения и юмор',
        'технологии': 'Технологии',
        'экономика': 'Экономика',
        'бизнес_стартапы': 'Бизнес и стартапы',
        'крипта': 'Криптовалюты',
        'путешествия': 'Путешествия',
        'маркетинг': 'Маркетинг, PR, реклама',
        'психология': 'Психология',
        'дизайн': 'Дизайн',
        'политика': 'Политика',
        'искусство': 'Искусство',
        'право': 'Право',
        'образование_познавательное': 'Образование и познавательное',
        'спорт': 'Спорт',
        'мода': 'Мода и красота',
        'здоровье_медицина': 'Здоровье и медицина',
        'картинки_фото': 'Картинки и фото',
        'приложения_софт': 'Софт и приложения',
        'фильмы': 'Видео и фильмы',
        'музыка': 'Музыка',
        'игры': 'Игры',
        'еда': 'Еда и кулинария',
        'цитаты': 'Цитаты',
        'рукоделие': 'Рукоделие',
        'финансы': 'Финансы',
        'шоубиз': 'Шоубиз',
        "Nan": 'Другое'}

    my_model_path = "models/checkpoint_best"

    df['label'] = 0
    df = df[['text', 'label']]

    test_dataset = Dataset.from_pandas(df.reset_index(drop=True))

    tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')

    def tokenize(batch):
        # print(batch)
        return tokenizer(batch["text"],
                         padding=True,
                         truncation=True,
                         max_length=256)

    test_dataset = test_dataset.map(tokenize, batched=True, batch_size=len(test_dataset))
    test_dataset.set_format("torch", columns=["input_ids", "label"])
    model = AutoModelForSequenceClassification.from_pretrained(my_model_path)

    # TrainingArguments
    training_args = TrainingArguments(
        output_dir='tmp',
        num_train_epochs=10,
        per_device_train_batch_size=20,
        per_device_eval_batch_size=20,
        evaluation_strategy="epoch",
        logging_dir=f"tmp/logs",
        logging_strategy="steps",
        logging_steps=10,
        learning_rate=5e-5,
        weight_decay=0.01,
        warmup_steps=500,
        save_strategy="epoch",
        load_best_model_at_end=True,
        save_total_limit=2,
        report_to="tensorboard",
        push_to_hub=False,
        #     hub_strategy="every_save",
        #     hub_model_id=repository_id,
        #     hub_token=HfFolder.get_token(),
    )

    trainer = Trainer(
        model=model,
        args=training_args,
        # other trainer settings
    )

    # Get predictions

    predictions = trainer.predict(test_dataset)
    predicted_labels = [np.argmax(pred) for pred in predictions.predictions]
    df['label'] = predicted_labels
    df['hard_label'] = df['label'].map(model.config.id2label)
    df['category'] = df['hard_label'].map(final_map)

    return df



