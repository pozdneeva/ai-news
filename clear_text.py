import pandas as pd
import re
import pymorphy2


def clear_alphabet(text):
    res = re.sub(r'\n', ' ', str(text))
    res = re.sub(r'[^a-zA-Zа-яА-Я0-9ёЁ ]', ' ', str(res))
    res = res.lower().split()
    res = ' '.join(res)
    return res


def remove_after_special_symbol(string):
    if 'подписывайся' in string:
        index = string.index('подписывайся')
        string = string[:index]
    return string


def pos(word, morth=pymorphy2.MorphAnalyzer()):
    "Return a likely part of speech for the *word*."""
    return morth.parse(word)[0].tag.POS


def clear_text(df):
    # удаляем ссылки и телефоны
    df['initial_text'] = df['text']
    df['text'] = df['text'].apply(lambda t: re.sub(r'\+?7\s?\(\d{3}\)\s?\d{3}-\d{2}-\d{2}', '',
                                                   re.sub(r'https?://\S+', '', str(t))).strip())
    df['text'] = df['text'].apply(clear_alphabet)
    df['text'] = df['text'].apply(remove_after_special_symbol)

    df['text_len'] = df['text'].apply(lambda x: len(x))
    df = df[(df['text_len'] > 40) & (df['text_len'] < 1106)].reset_index(drop=True)
    df = df.drop(columns='text_len')
    df = df.drop_duplicates()
    df = df[~df['text'].str.startswith('день ')]
    return df