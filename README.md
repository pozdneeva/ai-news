## Deploying a model 

Run in this in PowerSHell.

Please notice, that you have done this things:
- you have empty folder app/ in this project
- you have your input test file in folder data

```bash
docker build . -t  ai-news
```

```bash
docker run -it -p 8000:8000 -v "$(pwd)/app/:/app/" ai-news
```
