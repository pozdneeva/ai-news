FROM python:3.9-slim

COPY ./requirements.txt .

RUN pip install -r ./requirements.txt

COPY . .

CMD ["gunicorn", "--bind", "0.0.0.0", "--timeout",  "3600" , "--workers", "1", "--threads", "1",  "predict:app"]
