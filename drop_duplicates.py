import pandas as pd
import numpy as np
import datetime
import gc
from sklearn.metrics.pairwise import pairwise_distances
from sentence_transformers import SentenceTransformer
import warnings
warnings.filterwarnings('ignore')


def drop_duplicates_custom(df_input):

    # Drop duplicated text in your file
    # You need to enter the preprocessed file
    df = df_input.copy()[['text']]
    del df_input

    print(f'Our data has shape {df.shape} before drop duplicates')

    print('Start process with emd collection ---', datetime.datetime.today())
    embedder = SentenceTransformer('models/sentence-transformers_distiluse-base-multilingual-cased-v1')

    df['emb'] = df['text'].apply(lambda x: embedder.encode(x))
    df3 = pd.DataFrame(df['emb'].to_list(), columns=[f'emd_col_{x}' for x in range(512)])
    df = pd.concat([df, df3], axis=1)
    df = df.reset_index(drop=True)

    print('Start process with duplicates detection ---', datetime.datetime.today())

    # Clean up unnecessary data
    del df3
    gc.collect()

    train_clustring = df.loc[:, ['text'] + [f'emd_col_{x}' for x in range(512)]]
    final_res = pd.DataFrame()

    batch_size = train_clustring.shape[0] // 10

    for step in range(0, train_clustring.shape[0], batch_size):
        print(f'Processing data from {step} to {step + batch_size} ---', datetime.datetime.today())

        train_clustring_short = train_clustring.iloc[step:step + batch_size]

        res = pairwise_distances(train_clustring_short.iloc[:, 1:], train_clustring.iloc[:, 1:], metric='cosine')
        res = np.argwhere(res < 0.2)
        res = pd.DataFrame(res, columns=['text1_idx', 'text2_idx'])

        train_clustring_short['index'] = np.arange(train_clustring_short.shape[0])
        my_mapper_short = pd.Series(train_clustring_short['text'].values,
                                    index=train_clustring_short['index'].values).to_dict()

        res['text1'] = res['text1_idx'].map(my_mapper_short)
        res['text2'] = res['text2_idx'].map(df['text'])

        final_res = pd.concat([final_res, res])

    final_res = final_res.groupby('text1').apply(lambda x: x.loc[x['text2'].str.len().idxmax()])
    final_res = final_res.reset_index(drop=True)

    df = df[df['text'].isin(final_res['text2'])]

    print(f'Finished! Our data has shape {df.shape} before drop duplicates ---', datetime.datetime.today())

    return df[['text']]